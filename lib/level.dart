class Level {
  static final HIGH_SCHOOL = Level._("High School", "hs");
  static final MIDDLE_SCHOOL = Level._("Middle School", "ms");
  static final COLLEGE = Level._("College", "college");
  static final MAR_5K = Level._("5k-Marathon", "5kmarathon");

  static final values = [HIGH_SCHOOL, MIDDLE_SCHOOL, COLLEGE, MAR_5K];

  static final RUNNER_TYPE_LEVELS = [HIGH_SCHOOL, MIDDLE_SCHOOL, COLLEGE];

  final String _name;
  final String _endpoint;

  Level._(this._name, this._endpoint);

  static from(String name) {
    Level? level;
    for (final l in values) {
      if (name == l._name) {
        level = l;
        break;
      }
    }

    if (level == null) {
      throw Exception("Invalid name received: " + name);
    }
    return level;
  }

  String getName(){
    return _name;
  }

  String getEndpoint(){
    return _endpoint;
  }

  @override
  String toString(){
    return _name;
  }

}

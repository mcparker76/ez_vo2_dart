class PaceUnit {

  static final MILE = PaceUnit._("Mile");
  static final KM = PaceUnit._("KM");

  static final values = [MILE, KM];

  final String _name;

  PaceUnit._(this._name);

  static PaceUnit from(String unit){
    PaceUnit? paceUnit;

    for (final pu in values){
      if (pu._name == unit){
        paceUnit = pu;
        break;
      }
    }

    if (paceUnit == null){
      throw Exception("Invalid unit: " + unit);
    }

    return paceUnit;
  }

  String getName(){
    return _name;
  }

  @override
  String toString() {
    return _name;
  }

}
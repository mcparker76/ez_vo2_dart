import 'package:ez_vo2/time.dart';

class Race {

  static const _METERS_PER_MILE = 1609.344;

  final String _name;
  int _meters = 0;
  double _miles = 0;
  String? _validTime;

  Race(this._name, this._meters, this._miles, this._validTime);

  Race.meters(this._name, this._meters){
    _miles = _meters / _METERS_PER_MILE;
    _validTime = "";
  }

  Race.miles(this._name, this._miles){
    _meters = (_miles * _METERS_PER_MILE).toInt();
    _validTime = "";
  }

  String getName(){
    return _name;
  }

  int getMeters(){
    return _meters;
  }

  double getMiles(){
    return _miles;
  }

  Time getValidTime(){
    if (_validTime == null){
      throw Exception("valid time is null");
    }
    return Time(_validTime!);
  }

  @override
  bool operator ==(Object other) {
    bool isEqual = false;

    if (other is Race){
      isEqual = _meters == other._meters && _name == other._name;
    }

    return isEqual;
  }

  @override
  String toString(){
    return _name;
  }

}
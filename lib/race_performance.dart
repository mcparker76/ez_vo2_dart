import 'package:ez_vo2/race.dart';
import 'package:ez_vo2/time.dart';

class RacePerformance {

  Race? _race;
  Time? _time;

  RacePerformance(race, time){
    _race = race;
    _time = time;
  }

  Race getRace(){
    return _race!;
  }

  Time getTime(){
    return _time!;
  }

  @override
  bool operator ==(Object other) {
    bool isEqual = false;

    if (other is RacePerformance){
      isEqual = _race == other._race;
    }

    return isEqual;
  }


}
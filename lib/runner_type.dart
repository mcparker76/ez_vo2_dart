import 'dart:math';

import 'package:ez_vo2/race.dart';
import 'package:ez_vo2/runner_type_name.dart';

class RunnerType {

  RunnerTypeName _runnerTypeName;
  final double _aValue;
  final double _bValue;

  RunnerType(this._runnerTypeName, this._aValue, this._bValue);

  int getVo2Rate(Race race){
    return (100 * calculateVo2Rate(race)).round();
  }

  double calculateVo2Rate(Race race){
    return _aValue * pow(race.getMeters(), _bValue);
  }

  @override
  String toString(){
    return _runnerTypeName.getName();
  }

  @override
  bool operator ==(Object other) {
    bool isEqual = false;

    if (other is RunnerType){
      isEqual = _runnerTypeName == other._runnerTypeName;
    }

    return isEqual;
  }


}
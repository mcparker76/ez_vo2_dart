class RunnerTypeName {

  static final TYPE_800_1600 = RunnerTypeName._("800-1600", "8001600");
  static final TYPE_1600_3200 = RunnerTypeName._("1600-3200", "16003200");
  static final TYPE_800_1500 = RunnerTypeName._("800-1500", "8001500");
  static final TYPE_1500_5000 = RunnerTypeName._("1500-5000", "15005000");
  static final TYPE_5000_10000 = RunnerTypeName._("5000-10,000", "500010000");
  static final TYPE_5K_10K = RunnerTypeName._("5K-10K", "5k10k");
  static final TYPE_HALF_MARATHON = RunnerTypeName._("Half Marathon", "halfmarathon");
  static final TYPE_MARATHON = RunnerTypeName._("Marathon", "marathon");
  static final TYPE_CROSS_COUNTRY = RunnerTypeName._("Cross Country", "cc");

  static final values = [TYPE_800_1600, TYPE_1600_3200,
    TYPE_800_1500, TYPE_1500_5000, TYPE_5000_10000,
    TYPE_CROSS_COUNTRY,
    TYPE_5K_10K, TYPE_HALF_MARATHON, TYPE_MARATHON];

  final String _name;
  final String _endpoint;

  RunnerTypeName._(this._name, this._endpoint);

  String getName(){
    return _name;
  }

  String getEndpoint(){
    return _endpoint;
  }

  static RunnerTypeName from(String name){
    RunnerTypeName? typeName;

    for (RunnerTypeName rt in values){
      if (rt._name == name){
        typeName = rt;
        break;
      }
    }
    
    if (typeName == null){
      throw Exception(name + " is not valid runner type name");
    }
    return typeName;
  }

  @override
  String toString(){
    return _name;
  }

}
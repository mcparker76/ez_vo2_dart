import 'pace_unit.dart';

class Time {
  static const double _conversionFactor = 1000.0 / 1609.344;

  String _time = "";
  double _value = 0;
  int? _hours;
  int _minutes = 0;
  int _seconds = 0;

  PaceUnit _paceUnit = PaceUnit.MILE;

  Time(String time, [PaceUnit? paceUnit]){
    List<String> tokens = time.split(":");

    double value;

    switch (tokens.length){
      case 2:
        value = int.parse(tokens[0]) + int.parse(tokens[1]) / 60.0;
        break;
      case 3:
        value = 60 * int.parse(tokens[0]) +
            int.parse(tokens[1]) + int.parse(tokens[2]) / 60.0;
        break;
      default:
        throw Exception(time + " is not a valid time");
    }

    if (paceUnit != null){
      _paceUnit = paceUnit;
    }

    _time = _convertToString(value);
  }

  Time.value(double time, [PaceUnit? paceUnit]){
    _value = time;

    if (paceUnit != null){
      _paceUnit = paceUnit;
    }

    _time = _convertToString(time);
  }

  double toValue([PaceUnit? paceUnit]){
    double value;

    if (paceUnit != null){
      value = _convertToTime(paceUnit)._value;
    } else {
      value = _value;
    }

    return value;
  }

  @override
  String toString([PaceUnit? paceUnit]){
    String value;

    if (paceUnit != null){
      value = _convertToTime(paceUnit)._time;
    } else {
      value = _time;
    }

    return value;
  }

  @override
  bool operator ==(Object other) {
    bool isEqual = false;

    if (other is Time){
      if (_hours != null && other._hours != null){
        isEqual = (_hours == other._hours);
      }

      isEqual &= _minutes == other._minutes && _seconds == other._seconds;
    }

    return isEqual;
  }


  String _convertToString(double time){
    String convertedTime = "";

    _hours = null;
    double dblMinutes;

    if (time >= 60) {
      _hours = time ~/ 60.0;
      dblMinutes = time - (60 * _hours!);
    } else {
      dblMinutes = time;
    }

    _minutes = 60 * dblMinutes.toInt();
    _seconds = 60 * (dblMinutes - _minutes).toInt();

    if (_hours != null){
      convertedTime = _hours.toString() + ":"
          + _pad(_minutes) + ":"
          + _pad(_seconds);
    } else {
      convertedTime = _minutes.toString() + ":" + _pad(_seconds);
    }

    return convertedTime;
  }

  String _pad(int value){
    return value < 10 ? "0" + value.toString() : value.toString();
  }

  Time _convertToTime(PaceUnit paceUnit){
    Time convertedTime;

    if (_paceUnit == PaceUnit.MILE && paceUnit == PaceUnit.KM){
      convertedTime = Time.value(toValue() * _conversionFactor);
    } else if (_paceUnit == PaceUnit.KM && paceUnit == PaceUnit.MILE) {
      convertedTime = Time.value(toValue() / _conversionFactor);
    } else {
      convertedTime = this;
    }

    return convertedTime;
  }

}
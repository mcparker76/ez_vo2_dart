import 'training_type_name.dart';

class TrainingDetails {

  final int _minDuration;
  final int _maxDuration;
  final int _slowRate;
  final int _fastRate;
  final TrainingTypeName _trainingTypeName;

  TrainingDetails(this._minDuration, this._maxDuration, this._slowRate, this._fastRate, this._trainingTypeName);

  TrainingDetails.copy(TrainingDetails details, int minDuration, int maxDuration) :
        this(minDuration, maxDuration, details._slowRate, details._fastRate, details._trainingTypeName);

  int getMinDuration(){
    return _minDuration;
  }

  int getMaxDuration(){
    return _maxDuration;
  }

  int getSlowRate(){
    return _slowRate;
  }

  int getFastRate(){
    return _fastRate;
  }

  TrainingTypeName getTrainingTypeName(){
    return _trainingTypeName;
  }

  @override
  String toString() {
    return _trainingTypeName.getName();
  }

}
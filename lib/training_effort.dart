class TrainingEffort {

  static final MIN = TrainingEffort._("Min");
  static final MID = TrainingEffort._("Mid");
  static final MAX = TrainingEffort._("Max");

  static final values = [MIN, MID, MAX];

  final String _effort;

  TrainingEffort._(this._effort);

  String getEffort(){
    return _effort;
  }

  @override
  String toString(){
    return _effort;
  }
}
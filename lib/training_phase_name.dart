class TrainingPhaseName {

  static final TrainingPhaseName BASE = TrainingPhaseName._(1, "Base");
  static final TrainingPhaseName EARLY = TrainingPhaseName._(2, "Early");
  static final TrainingPhaseName COMPETITION = TrainingPhaseName._(3, "Competition");
  static final TrainingPhaseName PEAK = TrainingPhaseName._(4, "Peak");

  static final PHASES_1_TO_3 = [BASE, EARLY, COMPETITION];
  static final PHASES_2_TO_4 = [EARLY, COMPETITION, PEAK];

  final int _phaseNumber;
  final String _name;

  TrainingPhaseName._(this._phaseNumber, this._name);

  @override
  String toString() {
    return _phaseNumber.toString() + "-" + _name;
  }

}
import 'training_type_name.dart';

class TrainingRate implements Comparable{

  TrainingTypeName trainingTypeName;
  int slowRate;
  int fastRate;

  TrainingRate(this.trainingTypeName, this.slowRate, this.fastRate);

  @override
  int compareTo(other) {
    return slowRate.compareTo(other);
  }

  @override bool operator ==(Object other) {
    bool isEqual = false;
    if (other is TrainingRate){
      TrainingRate that = other;
      isEqual = trainingTypeName == that.trainingTypeName;
    }

    return isEqual;
  }

  @override
  String toString(){
    return trainingTypeName.getName();
  }

}
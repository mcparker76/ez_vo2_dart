class TrainingType{

  final String _name;
  final String _description;
  final String _purpose;
  final int _minSlowRate;
  final int _maxFastRate;

  TrainingType(this._name, this._description, this._purpose, this._minSlowRate, this._maxFastRate);

  @override
  String toString(){
    return _name;
  }

  String getName(){
    return _name;
  }

  String getDescription(){
    return _description;
  }

  String getPurpose(){
    return _purpose;
  }

  int getMinSlowRate(){
    return _minSlowRate;
  }

  int getMaxFastRate(){
    return _maxFastRate;
  }


}
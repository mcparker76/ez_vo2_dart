class TrainingTypeName {

  static final EASY = TrainingTypeName._("Easy");
  static final LONG = TrainingTypeName._("Long");
  static final TEMPO = TrainingTypeName._("Tempo");
  static final INTERVAL = TrainingTypeName._("Interval");
  static final MARATHON = TrainingTypeName._("Marathon");
  static final LACTIC = TrainingTypeName._("Lactic");

  static final NON_MARATHON_5K = [EASY, LONG, TEMPO, INTERVAL, LACTIC];
  static final MARATHON_5K = [EASY, LONG, MARATHON, TEMPO, INTERVAL];

  static final values = [EASY, LONG, TEMPO, INTERVAL, MARATHON, LACTIC];

  final String _name;

  TrainingTypeName._(this._name);

  String getName(){
    return _name;
  }

  @override
  String toString() {
    return _name;
  }

}